#include <Streaming.h>
#include <Scheduler.h>

const int led = 13;
long interval = 500;        // intervalo de tiempo entre cambios
                            // de estado del led
boolean led_value;

void setup() {
    Serial.begin(9600);
    pinMode(led, OUTPUT);

    led_value = false;

    Scheduler.startLoop(ingresarDatos);

    Serial << "Frecuencia actual: " << interval << endl;
    Serial << "Seleccione una nueva frecuencia: " << endl;
}

void loop() {
    led_value = !led_value;
    digitalWrite(led, led_value);
    delay(interval);
}

void ingresarDatos() {
    if (Serial.available()) {
        interval = (int) Serial.parseInt();
        Serial << "Nueva frecuencia: " << interval << endl;
    }
    yield();
}
