#include <Streaming.h>
#include <Scheduler.h>

const int led = 13;
const int led_rx = 72;
const int led_tx = 73;

struct tarea_t {
    long c; // WCET
    long d; // Vencimiento
    long t; // Período
};

boolean led_value;
boolean rx_value;
boolean tx_value;

tarea_t task1 = {1, 3, 3};
tarea_t task2 = {1, 4, 4};
tarea_t task3 = {1, 6, 6};

void setup() {
    Serial.begin(9600);

    pinMode(led, OUTPUT);
    pinMode(led_rx, OUTPUT);
    pinMode(led_tx, OUTPUT);

    led_value = HIGH;
    rx_value = LOW;
    tx_value = LOW;

    Scheduler.startLoop(task2_loop);
    Scheduler.startLoop(task3_loop);
}

void loop() {
    task_exec(task1, led, led_value);
}

void task2_loop() {
    task_exec(task2, led_rx, rx_value);
}

void task3_loop() {
    task_exec(task3, led_tx, tx_value);
}

void task_exec(tarea_t task, int led, boolean value) {
    digitalWrite(led, value);
    delay(task.c * 1000);
    digitalWrite(led, !value);
    delay((task.d - task.c) * 1000);
}
