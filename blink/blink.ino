/**
 * Hello World!
 * 
 * Trabajo Práctico 3
 * Ejercicio 3
 * 
 * Prender la luz de un led a intervalos regulares sin usar
 * la función `delay`.
 * 
 * La idea principal es simple, llevar registro del tiempo 
 * que pasó desde la última vez que cambió el estado del led.
 * 
 * Si ese tiempo supera un cierto intervalo, entonces el estado
 * del led cambia.
 */
#include <Streaming.h>

const int led = 13;

long interval = 500;        // intervalo de tiempo entre cambios
                            // de estado del led
unsigned long curr;
unsigned long prev;

boolean led_value;
boolean flag_read;

void setup() {
    Serial.begin(9600);

    pinMode(led, OUTPUT);

    led_value = false;
    flag_read = false;

    prev = 0; // inicio contador de tiempo en 0
    Serial << "Frecuencia actual: " << interval << endl;
    Serial << "Presione ´a´ para ingresar una nueva frecuencia." << endl;
}

void promptFrecuencia() {
    Serial << "Seleccione la nueva frecuencia: " << endl;
    Serial << "1) 100ms" << endl;
    Serial << "2) 250ms" << endl;
    Serial << "3) 500ms" << endl << endl;
}

void loop() {
    curr = millis(); // tomo tiempo actual

    if(curr - prev > interval) {
        led_value = !led_value;
        digitalWrite(led, led_value);
        prev = millis(); // reinicio contador de tiempo
    }
}

void clickButton1() {
    if(!flag_read) {
        flag_read = true;
    }
}

void serialEvent() {
    char inChar;
    char t;

    while (Serial.available()) {
        if(flag_read) {
            inChar = (char) Serial.read();
            switch(inChar) {
                case '1': interval = 100; break;
                case '2': interval = 250; break;
                case '3': interval = 500; break;
            }

            Serial << "Nueva frecuencia: " << interval << endl << endl;
            Serial << "Presione ´a´ para ingresar una nueva frecuencia.";
            Serial << endl;
            flag_read = false;
        }

        t = Serial.read();
        if((char) t == 'a') {
            promptFrecuencia();
            flag_read = true;
        }
    }
}
