#include <Streaming.h>
#include <Scheduler.h>

const int led = 13;
const int led_rx = 72;
const int led_tx = 73;

long interval = 500;

boolean led_value;
boolean rx_value;
boolean tx_value;

void setup() {
    Serial.begin(9600);

    pinMode(led, OUTPUT);
    pinMode(led_rx, OUTPUT);
    pinMode(led_tx, OUTPUT);
    
    led_value = LOW;
    rx_value = HIGH;
    tx_value = HIGH;
}

void loop() {
    digitalWrite(led, led_value);
    digitalWrite(led_rx, rx_value);
    digitalWrite(led_tx, tx_value);
    delay(interval);

    digitalWrite(led, !led_value);
    digitalWrite(led_rx, !rx_value);
    digitalWrite(led_tx, !tx_value);
    delay(interval);
}
